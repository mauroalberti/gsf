#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

requirements = [
        'numpy',
        'affine',
        'setuptools',
    ]

with open("README.md", "r") as readme_file:
    readme = readme_file.read()

setup(
    name='pygsf',
    version='7.0.0',
    author="Mauro Alberti",
    author_email="alberti.m65@gmail.com",
    description="gsf, a library for structural geology",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mauroalberti/gsf.git",
    project_urls={
        "Bug Tracker": "https://gitlab.com/mauroalberti/gsf/-/issues",
        "Documentation": "https://gitlab.com/mauroalberti/gsf/-/blob/master/README.md",
        "Source Code": "https://gitlab.com/mauroalberti/gsf/-/tree/master",
    },
    packages=find_packages(exclude=("tests",)),
    install_requires=requirements,
    license="GPL-3",
    classifiers=[
                   "Development Status :: 6 - Stable",
                   "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
                   "Operating System :: OS Independent",
                   "Intended Audience :: Developers",
                   "Intended Audience :: Science/Research",
                   "Programming Language :: Python :: 3",
                   "Programming Language :: Python :: 3 :: Only",
                   "Topic :: Software Development",
                   "Topic :: Scientific/Engineering :: GIS",
                   "Topic :: Scientific/Engineering :: Structural Geology",
                   "Topic :: Utilities"
               ],
    python_requires='>=3',
    tests_require=['pytest'],
)